﻿using System;
using System.IO;
using Deputados.iOS;
using Deputados.SourceCode.Interfaces;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_iOS))]
namespace Deputados.iOS
{
    public class SQLite_iOS : ISQLite
    {
        const string nomeDoAquivoDB = "Deputados.db3";

        public SQLiteConnection PegarConexao()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            path = Path.Combine(path, nomeDoAquivoDB);
            return new SQLiteConnection(path);
        }
    }
}
