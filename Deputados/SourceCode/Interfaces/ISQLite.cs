﻿using SQLite;

namespace Deputados.SourceCode.Interfaces
{
    public interface ISQLite
    {
        SQLiteConnection PegarConexao();
    }
}
