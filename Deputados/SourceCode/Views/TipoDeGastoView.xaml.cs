﻿using System;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class TipoDeGastoView : ContentPage
    {
        public TipoDeGastoViewModel ViewModel { get; set; }

        public TipoDeGastoView(string id)
        {
            InitializeComponent();
            ViewModel = new TipoDeGastoViewModel(id);
            BindingContext = ViewModel;

            Title = "Tipos De Gasto";

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
            await ViewModel.GetTiposDeGastoAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers()
        {
            MessagingCenter.Subscribe<ArgumentException>(this, "GetTiposDeGastoAsync", (msg) =>
            {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
        }

        void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<ArgumentException>(this, "GetTiposDeGastoAsync");
        }
    }
}
