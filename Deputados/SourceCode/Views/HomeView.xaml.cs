﻿using System;
using Deputados.SourceCode.Interfaces;
using Deputados.SourceCode.Models;
using Deputados.SourceCode.Utils;
using Deputados.SourceCode.ViewModels;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class HomeView : ContentPage
    {
        public HomeViewModel ViewModel { get; set; }

        public HomeView()
        {
            InitializeComponent();
            ViewModel = new HomeViewModel();
            BindingContext = ViewModel;
            
            AdicionarBotaoDeFiltro();
            RemoverHighlightDaListView();

            Title = "Deputados";
        }

        void RemoverHighlightDaListView()
        {
            lv.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };
        }

        void AdicionarBotaoDeFiltro()
        {
            var btnFiltro = new ToolbarItem { Command = ViewModel.EstadoCommand, Text = "ESTADO", Icon = "icon_filter.png" };
            ToolbarItems.Add(btnFiltro);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
            if (ViewModel.Deputados.Count.Equals(0))
            {
                await ViewModel.GetDeputadosAsync();
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers()
        {
            MessagingCenter.Subscribe<string>(this, "filtro", (estado) =>
            {
                PopupNavigation.Instance.PushAsync(new PopupEstados());
            });

            MessagingCenter.Subscribe<string>(this, "EstadoSelecionado", ViewModel.AplicarFiltroPorEstado);

            MessagingCenter.Subscribe<Deputado>(this, "DeputadoSelecionado", (dep) =>
            {
                Navigation.PushAsync(new DatalheDeputadoView(dep));
            });

            MessagingCenter.Subscribe<ArgumentException>(this, "FalhaGetDeputados", (msg) =>
            {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
        }

        void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<ArgumentException>(this, "FalhaGetDeputados");
            MessagingCenter.Unsubscribe<Deputado>(this, "DeputadoSelecionado");
            MessagingCenter.Unsubscribe<string>(this, "EstadoSelecionado");
            MessagingCenter.Unsubscribe<string>(this, "filtro");
        }
    }
}
