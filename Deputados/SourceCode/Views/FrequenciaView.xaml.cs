﻿using System;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class FrequenciaView : ContentPage
    {
        public FrequenciaViewModel ViewModel { get; set; }

        public FrequenciaView(string id)
        {
            InitializeComponent();
            ViewModel = new FrequenciaViewModel(id);
            BindingContext = ViewModel;

            Title = "Frequência";

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
            await ViewModel.GetFrequenciaDoDeputadoAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers()
        {
            MessagingCenter.Subscribe<ArgumentException>(this, "FalhaGetFrequencias", (msg) =>
            {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
        }

        void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<ArgumentException>(this, "FalhaGetFrequencias");
        }
    }
}
