﻿using System;
using System.Threading.Tasks;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views {
    public partial class GastosView : ContentPage {
        public GastoViewModel ViewModel { get; set; }
        string AnoSelecionado { get; set; }

        public GastosView(string id, string ano) {

            InitializeComponent();
            ViewModel = new GastoViewModel(id);
            BindingContext = ViewModel;

            Title = "Gastos Por Ano";

            //O ano deve ser entre 2009 e 2013
            Anos.ItemsSource = new string[] { "2009", "2010", "2011", "2012", "2013" };
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e) {
            AnoSelecionado = Anos.Items[Anos.SelectedIndex];
        }

        async Task Handle_ClickedAsync(object sender, System.EventArgs e) {
            await ViewModel.GetGastosAsync(AnoSelecionado);
        }


        protected override void OnAppearing() {
            base.OnAppearing();

            Subscribers();
        }

        protected override void OnDisappearing() {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers() {
            MessagingCenter.Subscribe<string>(this, "GetGastosAno", (msg) => {
                DisplayAlert("Atenção", "Selecione o ano para prosseguir.", "Ok");
            });
            MessagingCenter.Subscribe<ArgumentException>(this, "GetGastosAsync", (msg) => {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
            MessagingCenter.Subscribe<string>(this, "GetGastosVazio", (msg) => {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
        }

        void Unsubscribers() {
            MessagingCenter.Unsubscribe<ArgumentException>(this, "GetGastosAno");
            MessagingCenter.Unsubscribe<ArgumentException>(this, "GetGastosAsync");
            MessagingCenter.Unsubscribe<ArgumentException>(this, "GetGastosVazio");

        }
    }
}
