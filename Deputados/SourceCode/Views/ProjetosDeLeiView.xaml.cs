﻿using System;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class ProjetosDeLeiView : ContentPage
    {
        public ProjetosDeLeiViewModel ViewModel { get; set; }

        public ProjetosDeLeiView(string id)
        {
            InitializeComponent();
            ViewModel = new ProjetosDeLeiViewModel(id);
            BindingContext = ViewModel;

            Title = "Projetos De Lei";

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
            await ViewModel.GetProjetosDeLeiDoDeputadoAsync();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers()
        {
            MessagingCenter.Subscribe<ArgumentException>(this, "GetProjetosDeLeiDoDeputadoAsync", (msg) =>
            {
                DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");
            });
        }

        void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<ArgumentException>(this, "GetProjetosDeLeiDoDeputadoAsync");
        }
    }
}
