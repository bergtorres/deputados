﻿using System;
using System.Collections.Generic;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class ComissaoView : ContentPage
    {
        public ComissaoViewModel ViewModel { get; set; }

        public ComissaoView(string id)
        {
            InitializeComponent();
            ViewModel = new ComissaoViewModel(id);             BindingContext = ViewModel;

            Title = "Comissões";
        }
         protected override async void OnAppearing()         {             base.OnAppearing();              Subscribers();             await ViewModel.GetComissoesDoDeputadoAsync();         }          protected override void OnDisappearing()         {             base.OnDisappearing();             Unsubscribers();         }          void Subscribers()         {             MessagingCenter.Subscribe<ArgumentException>(this, "FalhaGetComissoes", (msg) =>             {                 DisplayAlert("Atenção", "Os dados estão indisponíveis no momento", "Ok");             });         }          void Unsubscribers()         {             MessagingCenter.Unsubscribe<ArgumentException>(this, "FalhaGetComissoes");         }
    }
}