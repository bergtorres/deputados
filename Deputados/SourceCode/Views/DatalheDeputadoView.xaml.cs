﻿using System;
using System.Diagnostics;
using Deputados.SourceCode.Models;
using Deputados.SourceCode.ViewModels;
using Xamarin.Forms;

namespace Deputados.SourceCode.Views
{
    public partial class DatalheDeputadoView : ContentPage
    {
        public DatalheDeputadoViewModel ViewModel { get; set; }

        public DatalheDeputadoView(Deputado deputado)
        {
            InitializeComponent();
            ViewModel = new DatalheDeputadoViewModel(deputado);
            BindingContext = ViewModel;

            Title = "Deputado";

            Foto.Source = new UriImageSource {
                Uri = new Uri(deputado.FotoURL),
                CachingEnabled = true,
                CacheValidity = new TimeSpan(5, 0, 0, 0)
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        void Subscribers()
        {
            MessagingCenter.Subscribe<Deputado>(this, "NavFrequencia", (dep) =>
            {
                Navigation.PushAsync(new FrequenciaView(dep.Id));
            });
            MessagingCenter.Subscribe<Deputado>(this, "NavComissoes", (dep) =>
            {
                Navigation.PushAsync(new ComissaoView(dep.Id));
            });
            MessagingCenter.Subscribe<Deputado>(this, "NavProjetos", (dep) =>
            {
                Navigation.PushAsync(new ProjetosDeLeiView(dep.Id));
            });
            MessagingCenter.Subscribe<Deputado>(this, "NavTipos", (dep) =>
            {
                Navigation.PushAsync(new TipoDeGastoView(dep.Id));
            });
            MessagingCenter.Subscribe<Deputado>(this, "NavGastos", (dep) =>
            {
                Navigation.PushAsync(new GastosView(dep.Id, "2010"));
            });
        }

        void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<Deputado>(this, "NavFrequencia");
            MessagingCenter.Unsubscribe<Deputado>(this, "NavComissoes");
            MessagingCenter.Unsubscribe<Deputado>(this, "NavProjetos");
            MessagingCenter.Unsubscribe<Deputado>(this, "NavTipos");
            MessagingCenter.Unsubscribe<Deputado>(this, "NavGastos");
        }
    }
}
