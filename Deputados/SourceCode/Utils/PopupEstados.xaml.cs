﻿using System.Diagnostics;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Deputados.SourceCode.Utils
{
    public partial class PopupEstados : PopupPage
    {
        public PopupEstados()
        {
            InitializeComponent();

            lvEstado.ItemsSource = new string[]{
                "Brasil",
                "AC",
                "AL",
                "AM",
                "AP",
                "BA",
                "CE",
                "DF",
                "ES",
                "GO",
                "MA",
                "MT",
                "MS",
                "MG",
                "PA",
                "PB",
                "PE",
                "PI",
                "PR",
                "RJ",
                "RN",
                "RO",
                "RR",
                "RS",
                "SC",
                "SE",
                "SP",
                "SO"
            };

            lvEstado.ItemTapped += (sender, e) => {
                var estado = e.Item as string;
                MessagingCenter.Send(estado, "EstadoSelecionado");
                PopupNavigation.Instance.PopAsync(true);
            };
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            PopupNavigation.Instance.PopAsync(true);
        }
    }
}
