﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Deputados.SourceCode.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        // Caso não seja informado o nome da variável, será utilizado o nome da variável que chamou a função
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            // NULO CONDICIONAL ? =>' (if PropertyChanged != null)
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs((propertyName)));
        }
    }
}
