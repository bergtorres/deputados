﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels
{
    public class ComissaoViewModel : BaseViewModel
    {
        const string URL_COMISSOES = "http://meucongressonacional.com/api/001/deputado/{0}/comissoes";

        ObservableCollection<Comissao> comissoes;
        public ObservableCollection<Comissao> Comissoes
        {
            get { return comissoes; }
            set
            {
                comissoes = value;
                OnPropertyChanged();
            }
        }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }
        bool titulo { get; set; }
        public bool Titulo { get { return titulo; } set { titulo = value; OnPropertyChanged(); } }

        string IdDeputado { get; set; }

        public ComissaoViewModel(string id)
        {
            IdDeputado = id;
            Comissoes = new ObservableCollection<Comissao>();
        }

        public async Task GetComissoesDoDeputadoAsync()
        {
            Carregando = true;
            Titulo = false;
            var urlFinal = string.Format(URL_COMISSOES, IdDeputado);
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(urlFinal);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonComissoes = request.Content.ReadAsStringAsync();
                        var arrayDeComissoes = JsonConvert.DeserializeObject<Comissao[]>(jsonComissoes.Result);

                        foreach (Comissao comissao in arrayDeComissoes)
                        {
                            AjustesNaComissao(comissao);
                        }
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "FalhaGetComissoes");
                    }
                    Titulo = true;
                    Carregando = false;
                }
                catch (Exception erro)
                {
                    Carregando = false;
                    Titulo = false;
                    MessagingCenter.Send(new ArgumentException(), "FalhaGetComissoes");
                    Debug.WriteLine(@"****** ERRO GetComissoesDoDeputadoAsync ****** " + erro);
                }
            }
        }

        void AjustesNaComissao(Comissao comissao)
        {
            comissao.NomeComissao = comissao.SiglaComissao + " - " + comissao.NomeComissao;
            Comissoes.Add(comissao);
        }
    }
}
