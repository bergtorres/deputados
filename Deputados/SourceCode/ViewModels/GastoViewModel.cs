﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels {
    public class GastoViewModel : BaseViewModel {

        const string URL_FREQUENCIA = "http://meucongressonacional.com/api/001/deputado/{0}/gastos/{1}";

        ObservableCollection<Gasto> gastos;
        public ObservableCollection<Gasto> Gastos {
            get {
                return gastos;
            }
            set {
                gastos = value; OnPropertyChanged();
            }
        }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }

        string IdDeputado { get; set; }

        public GastoViewModel(string id) {
            IdDeputado = id;
            gastos = new ObservableCollection<Gasto>();
        }

        public async Task GetGastosAsync(string Ano) {
            if (string.IsNullOrEmpty(Ano)) {
                MessagingCenter.Send("", "GetGastosAno");
            } else {
                Carregando = true;
                var urlFinal = string.Format(URL_FREQUENCIA, IdDeputado, Ano);
                using (var client = new HttpClient()) {
                    try {
                        var request = await client.GetAsync(urlFinal);
                        if (request.IsSuccessStatusCode) {
                            var jsonGastos = request.Content.ReadAsStringAsync();
                            var settings = new JsonSerializerSettings {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            var arrayDeGastos = JsonConvert.DeserializeObject<Gasto[]>(jsonGastos.Result, settings);
                            Gastos.Clear();

                            if (arrayDeGastos.Length.Equals(0)) {
                                MessagingCenter.Send("", "GetGastosVazio");
                            } else {
                                foreach (Gasto gasto in arrayDeGastos) {
                                    gasto.Valor = FormatarValor(gasto.Valor);
                                    Gastos.Add(gasto);
                                }
                            }
                        } else {
                            MessagingCenter.Send(new ArgumentException(), "GetGastosAsync");
                        }
                        Carregando = false;
                    } catch (Exception erro) {
                        Carregando = false;
                        MessagingCenter.Send(new ArgumentException(), "GetGastosAsync");
                        Debug.WriteLine(@"****** ERRO GetGastosAsync ****** " + erro);
                    }
                }
            }
        }

        float FormatarValor(float Valor)
        {
            var FormattedPrice = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:N}", Valor);
            var toFloat = float.Parse(FormattedPrice, CultureInfo.GetCultureInfo("pt-BR"));

            return toFloat;
        }
    }
}
