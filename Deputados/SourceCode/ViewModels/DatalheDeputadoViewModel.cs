﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Input;
using Deputados.SourceCode.Dao;
using Deputados.SourceCode.Interfaces;
using Deputados.SourceCode.Models;
using Deputados.SourceCode.Utils;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels {
    public class DatalheDeputadoViewModel {

        public ICommand FrequenciaCommand { get; set; }
        public ICommand ComissoesCommand { get; set; }
        public ICommand ProjetosCommand { get; set; }
        public ICommand TiposCommand { get; set; }
        public ICommand GastosCommand { get; set; }

        public string Id { get; set; }
        public string NomeCompleto { get; set; }
        public string Cargo { get; set; }
        public string Partido { get; set; }
        public string Uf { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string FotoURL { get; set; }
        public string GastoPorDia { get; set; }
        public string GastoTotal { get; set; }

        public DatalheDeputadoViewModel(Deputado deputado) {
            Id = deputado.Id;
            NomeCompleto = deputado.NomeCompleto;
            Cargo = deputado.Cargo;
            Partido = deputado.Partido;
            Uf = deputado.Uf;
            Telefone = deputado.Telefone;
            Email = deputado.Email;
            FotoURL = deputado.FotoURL;
            GastoPorDia = FormatarValor(deputado.GastoPorDia);
            GastoTotal = FormatarValor(deputado.GastoTotal);

            FrequenciaCommand = new Command(() => {
                NavegarParaOutraTela("NavFrequencia", deputado);
            });
            ComissoesCommand = new Command(() => {
                NavegarParaOutraTela("NavComissoes", deputado);

            });
            ProjetosCommand = new Command(() => {
                NavegarParaOutraTela("NavProjetos", deputado);
            });
            TiposCommand = new Command(() => {
                NavegarParaOutraTela("NavTipos", deputado);
            });
            GastosCommand = new Command(() => {
                NavegarParaOutraTela("NavGastos", deputado);
            });
        }

        void NavegarParaOutraTela(string nav, Deputado dep) {
            MessagingCenter.Send(dep, nav);
        }

        string FormatarValor(float Valor)
        {
            var FormattedPrice = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:N}", Valor);
            return "R$ " + FormattedPrice;
        }
    }
}
