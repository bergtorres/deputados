﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels
{
    public class FrequenciaViewModel : BaseViewModel
    {
        const string URL_FREQUENCIA = "http://meucongressonacional.com/api/001/deputado/{0}/freq";

        ObservableCollection<Frequencia> frequencias;
        public ObservableCollection<Frequencia> Frequencias
        {
            get { return frequencias; }
            set { frequencias = value; OnPropertyChanged(); }
        }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }
        bool titulo { get; set; }
        public bool Titulo { get { return titulo; } set { titulo = value; OnPropertyChanged(); } }

        string IdDeputado { get; set; }

        public FrequenciaViewModel(string id)
        {
            IdDeputado = id;
            Frequencias = new ObservableCollection<Frequencia>();
        }

        public async Task GetFrequenciaDoDeputadoAsync()
        {
            Carregando = true;
            Titulo = false;
            var urlFinal = string.Format(URL_FREQUENCIA, IdDeputado);
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(urlFinal);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonFrequencias = request.Content.ReadAsStringAsync();
                        var arrayDeFrequencias = JsonConvert.DeserializeObject<Frequencia[]>(jsonFrequencias.Result);

                        foreach (Frequencia freq in arrayDeFrequencias)
                        {
                            AjustesNoFormatoDosDados(freq);
                        }
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "FalhaGetFrequencias");
                    }
                    Titulo = true;
                    Carregando = false;
                }
                catch (Exception erro)
                {
                    Carregando = false;
                    Titulo = false;
                    MessagingCenter.Send(new ArgumentException(), "FalhaGetFrequencias");
                    Debug.WriteLine(@"****** ERRO GetFrequenciaDoDeputadoAsync ****** " + erro);
                }
            }
        }

        void AjustesNoFormatoDosDados(Frequencia freq)
        {
            var indiceDeAusencia = 100 - freq.IndicePresenca;

            var percentualPresencao = string.Format("{0:N}", freq.IndicePresenca);
            var percentualAusencia = string.Format("{0:N}", indiceDeAusencia);

            freq.IndicePresenca = float.Parse(percentualPresencao, CultureInfo.GetCultureInfo("pt-BR"));
            freq.IndiceAusencia = float.Parse(percentualAusencia, CultureInfo.GetCultureInfo("pt-BR"));

            Frequencias.Add(freq);
        }
    }
}
