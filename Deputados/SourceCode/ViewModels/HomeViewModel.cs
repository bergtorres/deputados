﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Deputados.SourceCode.Dao;
using Deputados.SourceCode.Interfaces;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        const string URL_TODOS_OS_DEPUTADOS = "http://meucongressonacional.com/api/001/deputado";

        ObservableCollection<Deputado> TodosOsDeputados;

        ObservableCollection<Deputado> deputados;
        public ObservableCollection<Deputado> Deputados
        {
            get { return deputados; }
            set { deputados = value; OnPropertyChanged(); }
        }

        string subtitulo { get; set; }
        public string Subtitulo
        {
            get { return subtitulo; }
            set
            {
                subtitulo = value; OnPropertyChanged();
            }
        }

        Deputado deputadoSelecionado;
        public Deputado DeputadoSelecionado
        {
            get { return deputadoSelecionado; }
            set
            {
                deputadoSelecionado = value;
                if (deputadoSelecionado != null)
                {
                    MessagingCenter.Send(deputadoSelecionado, "DeputadoSelecionado");
                }
            }
        }

        public ICommand EstadoCommand { get; set; }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }
        bool titulo { get; set; }
        public bool Titulo { get { return titulo; } set { titulo = value; OnPropertyChanged(); } }

        public HomeViewModel()
        {
            Deputados = new ObservableCollection<Deputado>();

            EstadoCommand = new Command(() =>
            {
                AbrirPopUpDeEstados();
            });
        }

        public async Task GetDeputadosAsync()
        {
            Carregando = true;
            Titulo = false;
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(URL_TODOS_OS_DEPUTADOS);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonDeputados = request.Content.ReadAsStringAsync();
                        var arrayDeDeputados = JsonConvert.DeserializeObject<List<Deputado>>(jsonDeputados.Result);

                        foreach (Deputado deputado in arrayDeDeputados)
                        {
                            Deputados.Add(deputado);
                        }

                        if (PegarDadosDoDB().Equals(0))
                        {
                            Debug.WriteLine(@"****** GetDeputadosAsync ****** ADD NO BD");
                            foreach (Deputado deputado in arrayDeDeputados)
                            {
                                SalvarDeputadoDB(deputado);
                            }
                        }
                        else
                        {
                            Debug.WriteLine(@"****** GetDeputadosAsync ******  NÃO ADD NO BD");
                        }

                        TodosOsDeputados = Deputados;
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "FalhaGetDeputados");
                    }
                    Carregando = false;
                    Titulo = true;
                }
                catch (Exception erro)
                {
                    Carregando = false;
                    Titulo = false;
                    MessagingCenter.Send(new ArgumentException(), "FalhaGetDeputados");
                    Debug.WriteLine(@"****** ERRO GetDeputadosAsync ****** " + erro);
                }
            }
        }

        void SalvarDeputadoDB(Deputado dep)
        {
            using (var conexao = DependencyService.Get<ISQLite>().PegarConexao())
            {
                DeputadoDAO deputadoDAO = new DeputadoDAO(conexao);
                Debug.WriteLine(dep.NomeCompleto);
                deputadoDAO.SalvarDeputado(dep);
            }
        }

        int PegarDadosDoDB()
        {
            using (var conexao = DependencyService.Get<ISQLite>().PegarConexao())
            {
                DeputadoDAO deputadoDAO = new DeputadoDAO(conexao);
                var deps = deputadoDAO.RecuperarDeputados();
                return deps.Count;
            }
        }

        void AbrirPopUpDeEstados()
        {
            MessagingCenter.Send("", "filtro");
        }

        public void AplicarFiltroPorEstado(string filter)
        {
            if (filter.Equals("Brasil"))
            {
                Deputados = TodosOsDeputados;
            }
            else
            {
                Deputados = new ObservableCollection<Deputado>(TodosOsDeputados.Where((dep) => dep.Uf.ToLower().Equals(filter.ToLower())));
            }
        }
    }
}
