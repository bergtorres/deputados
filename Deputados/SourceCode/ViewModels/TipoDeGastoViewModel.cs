﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Deputados.SourceCode.Dao;
using Deputados.SourceCode.Interfaces;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels
{
    public class TipoDeGastoViewModel : BaseViewModel
    {
        const string URL_FREQUENCIA = "http://meucongressonacional.com/api/001/deputado/{0}/gastos/tipo";

        List<TipoDeGasto> GastosDeTodosOsDeputados { get; set; }
        List<TipoDeGasto> GastosDoDeputado { get; set; }

        ObservableCollection<TipoDeGasto> tipoDeGastos;
        public ObservableCollection<TipoDeGasto> TipoDeGastos
        {
            get { return tipoDeGastos; }
            set { tipoDeGastos = value; OnPropertyChanged(); }
        }
        public Deputado[] GeDeputados { get; set; }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }
        bool titulo { get; set; }
        public bool Titulo { get { return titulo; } set { titulo = value; OnPropertyChanged(); } }

        string IdDeputado { get; set; }

        public TipoDeGastoViewModel(string id)
        {
            IdDeputado = id;
            TipoDeGastos = new ObservableCollection<TipoDeGasto>();
            GastosDeTodosOsDeputados = new List<TipoDeGasto>();
            GastosDoDeputado = new List<TipoDeGasto>();

        }

        public async Task GetTiposDeGastoAsync()
        {
            Carregando = true;
            Titulo = false;
            var urlFinal = string.Format(URL_FREQUENCIA, IdDeputado);
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(urlFinal);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonTiposDeGasto = request.Content.ReadAsStringAsync();
                        GastosDoDeputado = JsonConvert.DeserializeObject<List<TipoDeGasto>>(jsonTiposDeGasto.Result);
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "GetTiposDeGastoAsync");
                    }
                }
                catch (Exception erro)
                {
                    Carregando = false;
                    Titulo = false;
                    MessagingCenter.Send(new ArgumentException(), "GetTiposDeGastoAsync");
                    Debug.WriteLine(@"****** ERRO GetTiposDeGastoAsync ****** " + erro);
                }
            }
            await GetGastosAsync();
        }

        async Task GetGastosAsync()
        {
            var gastos = PegarGastosDoDB();
            if (gastos.Count.Equals(0))
            {
                var deputados = PegarDeputadosDoDB();
                foreach (Deputado dep in deputados)
                {
                    await GetTipoDeGastoDosDeputadosAsync(dep.Id);
                }
            }
            else
            {
                foreach (TipoDeGasto tipo in gastos)
                {
                    GastosDeTodosOsDeputados.Add(tipo);
                }
            }

            CompararGastos();
        }

        async Task GetTipoDeGastoDosDeputadosAsync(string id)
        {
            var urlFinal = string.Format(URL_FREQUENCIA, id);
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(urlFinal);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonTiposDeGasto = request.Content.ReadAsStringAsync();
                        var gastos = JsonConvert.DeserializeObject<List<TipoDeGasto>>(jsonTiposDeGasto.Result);

                        foreach (TipoDeGasto tipo in gastos)
                        {
                            tipo.Media = FormatarValor(tipo.Media);
                            tipo.Valor = FormatarValor(tipo.Valor);
                            GastosDeTodosOsDeputados.Add(tipo);
                            SalvarTipoDeGastoDB(tipo);
                        }
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "GetTiposDeGastoAsync");
                    }
                }
                catch (Exception erro)
                {
                    MessagingCenter.Send(new ArgumentException(), "GetTiposDeGastoAsync");
                    Debug.WriteLine(@"****** ERRO GetTipoDeGastoDosDeputadosAsync ****** " + erro);
                }
            }
        }

        void CompararGastos()
        {
            for (int i = 0; i < GastosDoDeputado.Count; i++)
            {

                var nome = GastosDoDeputado[i].Nome;
                var mediaDoDeputado = GastosDoDeputado[i].Media;

                float somaDeTodasAsMediasComEsseTipoDeGasto = 0;
                int qtdDeGastosDeTipo = 0;

                for (int j = 0; j < GastosDeTodosOsDeputados.Count; j++)
                {
                    var nomeD = GastosDeTodosOsDeputados[j].Nome;
                    var mediaDeOutroDeputado = GastosDeTodosOsDeputados[j].Media;

                    if (nomeD.Equals(nome))
                    {
                        qtdDeGastosDeTipo++;

                        somaDeTodasAsMediasComEsseTipoDeGasto = somaDeTodasAsMediasComEsseTipoDeGasto + mediaDeOutroDeputado;
                    }
                }
                var mediaGeral = somaDeTodasAsMediasComEsseTipoDeGasto / qtdDeGastosDeTipo;
                Debug.WriteLine(@"****** " + mediaGeral);
                AnalizarMediaDeGastos(mediaGeral, GastosDoDeputado[i]);
            }

            Carregando = false;
            Titulo = true;
        }

        void AnalizarMediaDeGastos(float mediaGeral, TipoDeGasto tipo)
        {
                tipo.MediaGeral = mediaGeral;
                TipoDeGastos.Add(tipo);
        }

        float FormatarValor(float Valor)
        {
            var FormattedPrice = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:N}", Valor);
            var toFloat = float.Parse(FormattedPrice, CultureInfo.GetCultureInfo("pt-BR"));

            return toFloat;
        }

        void SalvarTipoDeGastoDB(TipoDeGasto tp)
        {
            using (var conexao = DependencyService.Get<ISQLite>().PegarConexao())
            {
                TipoDeGastoDAO tipoDeGastoDAO = new TipoDeGastoDAO(conexao);
                tipoDeGastoDAO.SalvarTipoDeGasto(tp);
            }
        }

        List<TipoDeGasto> PegarGastosDoDB()
        {
            using (var conexao = DependencyService.Get<ISQLite>().PegarConexao())
            {
                TipoDeGastoDAO tipoDeGastoDAO = new TipoDeGastoDAO(conexao);
                var tips = tipoDeGastoDAO.RecuperarTipoDeGasto();
                return tips;
            }
        }

        List<Deputado> PegarDeputadosDoDB()
        {
            using (var conexao = DependencyService.Get<ISQLite>().PegarConexao())
            {
                DeputadoDAO deputadoDAO = new DeputadoDAO(conexao);
                var deps = deputadoDAO.RecuperarDeputados();
                return deps;
            }
        }
    }
}