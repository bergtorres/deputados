﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Deputados.SourceCode.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Deputados.SourceCode.ViewModels
{
    public class ProjetosDeLeiViewModel : BaseViewModel
    {
        const string URL_FREQUENCIA = "http://meucongressonacional.com/api/001/deputado/{0}/projetos";

        ObservableCollection<Projeto> projetos;
        public ObservableCollection<Projeto> Projetos
        {
            get { return projetos; }
            set { projetos = value; OnPropertyChanged(); }
        }

        bool carregando { get; set; }
        public bool Carregando { get { return carregando; } set { carregando = value; OnPropertyChanged(); } }
        bool titulo { get; set; }
        public bool Titulo { get { return titulo; } set { titulo = value; OnPropertyChanged(); } }

        string IdDeputado { get; set; }

        public ProjetosDeLeiViewModel(string id)
        {
            IdDeputado = id;
            Projetos = new ObservableCollection<Projeto>();
        }

        public async Task GetProjetosDeLeiDoDeputadoAsync()
        {
            Carregando = true;
            Titulo = false;
            var urlFinal = string.Format(URL_FREQUENCIA, IdDeputado);
            using (var client = new HttpClient())
            {
                try
                {
                    var request = await client.GetAsync(urlFinal);
                    if (request.IsSuccessStatusCode)
                    {
                        var jsonProjetos = request.Content.ReadAsStringAsync();
                        var arrayDeProjetos = JsonConvert.DeserializeObject<Projeto[]>(jsonProjetos.Result);

                        foreach (Projeto proj in arrayDeProjetos)
                        {
                            Projetos.Add(proj);
                        }
                    }
                    else
                    {
                        MessagingCenter.Send(new ArgumentException(), "GetProjetosDeLeiDoDeputadoAsync");
                    }
                    Carregando = false;
                    Titulo = true;
                }
                catch (Exception erro)
                {
                    Carregando = false;
                    Titulo = false;
                    MessagingCenter.Send(new ArgumentException(), "GetProjetosDeLeiDoDeputadoAsync");
                    Debug.WriteLine(@"****** ERRO GetProjetosDeLeiDoDeputadoAsync ****** " + erro);
                }
            }
        }
    }
}