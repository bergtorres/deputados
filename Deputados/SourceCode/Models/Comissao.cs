﻿namespace Deputados.SourceCode.Models
{
    public class Comissao
    {
        public string IdeCadastroDeputado { get; set; }
        public string SiglaComissao { get; set; }
        public string Entrada { get; set; }
        public string Condicao { get; set; }
        public string Saida { get; set; }
        public string NomeComissao { get; set; }
        public string Orgao { get; set; }
        public string EntradaTxt { get; set; }
        public string SaidaTxt { get; set; }
    }
}
