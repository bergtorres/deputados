﻿namespace Deputados.SourceCode.Models
{
    public class TipoDeGasto
    {
        public string Nome { get; set; }
        public float Valor { get; set; }
        public float Media { get; set; }
        public float MediaGeral { get; set; }
    }
}
