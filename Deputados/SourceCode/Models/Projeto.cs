﻿namespace Deputados.SourceCode.Models
{
    public class Projeto
    {
        public string Nome { get; set; }
        public string Ano { get; set; }
        public string IdParlamentarAutor { get; set; }
        public string NomeAutor { get; set; }
        public string DataApresentacao { get; set; }
        public string Sigla { get; set; }
        public string Ementa { get; set; }
    }
}      