﻿namespace Deputados.SourceCode.Models
{
    public class Frequencia
    {
        public string IdParlamentar { get; set; }
        public string Ano { get; set; }
        public string PresencasDias { get; set; }
        public string PresencasSessoes { get; set; }
        public string AusenciasDias { get; set; }
        public string AusenciasSessoes { get; set; }
        public float IndicePresenca { get; set; }
        public float IndiceAusencia { get; set; }

    }
}
