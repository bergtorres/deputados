﻿using SQLite;

namespace Deputados.SourceCode.Models
{
    public class Deputado
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string NomeParlamentar { get; set; }
        public string NomeCompleto { get; set; }
        public string Cargo { get; set; }
        public string Partido { get; set; }
        public string Mandato { get; set; }
        public string Sexo { get; set; }
        public string Uf { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Nascimento { get; set; }
        public string FotoURL { get; set; }
        public float GastoTotal { get; set; }
        public float GastoPorDia { get; set; }
    }
}
