﻿namespace Deputados.SourceCode.Models
{
    public class Gasto
    {
        public string CnpjCpf { get; set; }
        public string TipoGasto { get; set; }
        public string DescricaoGasto { get; set; }
        public string DataEmissao { get; set; }
        public float Valor { get; set; }
    }
}