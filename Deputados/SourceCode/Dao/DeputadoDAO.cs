﻿using System.Collections.Generic;
using Deputados.SourceCode.Models;
using SQLite;

namespace Deputados.SourceCode.Dao
{
    public class DeputadoDAO
    {
        readonly SQLiteConnection conexao;

        public DeputadoDAO(SQLiteConnection connection)
        {
            conexao = connection;
            conexao.CreateTable<Deputado>(); // Table criada apenas se não exisite (default)
        }

        public void SalvarDeputado(Deputado deputado)
        {
            conexao.Insert(deputado);
        }

        public List<Deputado> RecuperarDeputados()
        {
            return conexao.Table<Deputado>().ToList();
        }
    }
}
