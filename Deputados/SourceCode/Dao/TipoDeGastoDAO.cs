﻿using System.Collections.Generic;
using Deputados.SourceCode.Models;
using SQLite;

namespace Deputados.SourceCode.Dao
{
    public class TipoDeGastoDAO
    {
        readonly SQLiteConnection conexao;

        public TipoDeGastoDAO(SQLiteConnection connection)
        {
            conexao = connection;
            conexao.CreateTable<TipoDeGasto>(); // Table criada apenas se não exisite (default)
        }

        public void SalvarTipoDeGasto(TipoDeGasto tipo)
        {
            conexao.Insert(tipo);
        }

        public List<TipoDeGasto> RecuperarTipoDeGasto()
        {
            return conexao.Table<TipoDeGasto>().ToList();
        }
    }
}
