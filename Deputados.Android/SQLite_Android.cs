﻿using System.IO;
using Deputados.Droid;
using Deputados.SourceCode.Interfaces;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Android))]
namespace Deputados.Droid
{
    public class SQLite_Android : ISQLite
    {
        const string nomeDoAquivoDB = "Deputados.db3";

        public SQLiteConnection PegarConexao()
        {
            var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            path = Path.Combine(path, nomeDoAquivoDB);
            return new SQLiteConnection(path);
        }
    }
}
